package com.haladyj.SS19RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss19RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss19RsApplication.class, args);
	}

}
