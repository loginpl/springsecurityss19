package com.haladyj.SS19AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss19AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss19AsApplication.class, args);
	}

}
